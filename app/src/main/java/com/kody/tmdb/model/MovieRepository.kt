package com.kody.tmdb.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import androidx.room.Room
import com.kody.tmdb.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.function.Consumer
import javax.inject.Inject
import javax.inject.Singleton


class MovieRepository @Inject constructor(val dao: MovieDao): Callback<Movie?> {
    val service: MovieService

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://developers.themoviedb.org/3/")
                .build()

        service = retrofit.create(MovieService::class.java)
    }


    suspend fun getMovie(id: String): LiveData<Movie> = liveData {
        emitSource(dao.findById(id).asLiveData())
        val call = service.getMovie(BuildConfig.TMDB_API_KEY, id)
        call?.enqueue(this@MovieRepository)
    }

    override fun onResponse(call: Call<Movie?>, response: Response<Movie?>) {
        response.body()?.let {
            dao.insertAll(it)
        }
    }

    override fun onFailure(call: Call<Movie?>, t: Throwable) {
        TODO("Not yet implemented")
    }
}

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Provides
    fun provideMovieDao(appDatabase: MovieDatabase): MovieDao {
        return appDatabase.movieDao()
    }

    @Provides
    fun provideMovieResultDao(appDatabase: MovieDatabase): MovieResultDao {
        return appDatabase.movieResultDao()
    }

    @Provides
    fun provideMovieResultRepository(movieResultDao: MovieResultDao): MovieResultRepository {
        return MovieResultRepository(movieResultDao)
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): MovieDatabase {
        return Room.databaseBuilder(
            appContext,
            MovieDatabase::class.java,
            "MovieDb"
        ).build()
    }
}