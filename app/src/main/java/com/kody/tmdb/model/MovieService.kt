package com.kody.tmdb.model

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

typealias SortedByString = String

interface MovieService {
    @GET("/discover/movie-discover")
    fun getMovieResults(@Query("api_key") api_key: String, @Query("sort_by") sortBy: SortedByString): Call<List<MovieResult>?>?

    @GET("/movie/{id}")
    fun getMovie(@Query("api_key") api_key: String, @Path("id") id: String): Call<Movie?>?
}