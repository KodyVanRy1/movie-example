package com.kody.tmdb.model

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import kotlinx.coroutines.flow.Flow

@Database(entities = [Movie::class, MovieResult::class], version = 1)
abstract class MovieDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun movieResultDao(): MovieResultDao
}

@Dao
interface MovieDao {
    @Query("SELECT * FROM movie")
    fun getAll(): Flow<List<Movie>>

    @Query("SELECT * FROM movie WHERE id=:id")
    fun findById(id: String): Flow<Movie>

    @Insert(onConflict = REPLACE)
    fun insertAll(vararg movies: Movie)

    @Delete
    fun delete(movie: Movie)
}

@Dao
interface MovieResultDao {
    @Query("SELECT * FROM movieResult")
    fun getAll(): Flow<List<MovieResult>>

    @Query("SELECT * FROM movieResult WHERE id=:id")
    fun findById(id: String): MovieResult

    @Insert(onConflict = REPLACE)
    fun insertAll(vararg movies: MovieResult)

    @Delete
    fun delete(movie: MovieResult)
}

