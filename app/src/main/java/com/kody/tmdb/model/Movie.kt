package com.kody.tmdb.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Movie(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "poster_path") @SerializedName("poster_path") val poster: String?,
    @ColumnInfo(name = "original_title") @SerializedName("original_title") val title: String?
)

@Entity
data class MovieResult(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "poster_path") @SerializedName("poster_path") val poster: String?,
    @ColumnInfo(name = "original_title") @SerializedName("original_title") val title: String?
)