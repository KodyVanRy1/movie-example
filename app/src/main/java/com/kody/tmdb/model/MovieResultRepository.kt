package com.kody.tmdb.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import androidx.room.Room
import com.kody.tmdb.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.function.Consumer
import javax.inject.Inject
import javax.inject.Singleton


class MovieResultRepository @Inject constructor(val dao: MovieResultDao): Callback<List<MovieResult>?> {
    val service: MovieService

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://developers.themovieResultdb.org/3/")
                .build()

        service = retrofit.create(MovieService::class.java)
    }


    fun getMovieResults(): LiveData<List<MovieResult>> = liveData {
        emitSource(dao.getAll().asLiveData())
        val call = service.getMovieResults(BuildConfig.TMDB_API_KEY, "popularity.desc")
        call?.enqueue(this@MovieResultRepository)
    }

    override fun onResponse(call: Call<List<MovieResult>?>, response: Response<List<MovieResult>?>) {
        response.body()?.let {
            dao.insertAll(*(it.toTypedArray()))
        }
    }

    override fun onFailure(call: Call<List<MovieResult>?>, t: Throwable) {
        TODO("Not yet implemented")
    }
}