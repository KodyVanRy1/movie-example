package com.kody.tmdb.view

import android.widget.TextView
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.kody.tmdb.viewmodel.MovieResultViewModel

@Composable
fun MovieListScreen(navController: NavController, viewModel: MovieResultViewModel) {
    LazyColumn {
        items(
                items = viewModel.movieResults.value ?: listOf(),
                itemContent = { movieResult ->
                    Text(movieResult.title ?: "<Empty Title>")
                }
        )
    }
}