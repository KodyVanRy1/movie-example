package com.kody.tmdb.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.kody.tmdb.model.MovieResult
import com.kody.tmdb.model.MovieResultDao
import com.kody.tmdb.model.MovieResultRepository
import javax.inject.Inject

class MovieResultViewModel @Inject constructor(private val movieResultRepository: MovieResultRepository) : ViewModel() {

    val movieResults: LiveData<List<MovieResult>> = movieResultRepository.getMovieResults()

}