package com.kody.tmdb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.Composable
import androidx.navigation.NavHost
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.kody.tmdb.view.MovieListScreen
import com.kody.tmdb.viewmodel.MovieResultViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val movieResultViewModel by viewModels<MovieResultViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeNavigation(movieResultViewModel)
        }
    }
}

@Composable
fun ComposeNavigation(movieResultViewModel: MovieResultViewModel) {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = "movie_list"
    ) {
        composable("movie_list") {
            MovieListScreen(navController = navController, viewModel = movieResultViewModel)
        }
//        composable("movie_details") {
//            MovieDetailsScreen(navController = navController)
//        }
    }
}
